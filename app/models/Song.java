package models;
import io.ebean.*;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import javax.validation.Constraint;
import java.util.HashSet;
import java.util.*;
/**
 * User entity managed by Ebean.
 * @author David Gutierrez
 */
@Entity
public class Song extends Model {

    @Id
    public Integer id;
    //@Constraints.Required
    public String title;
    public String url;
    //@Constraints.Required
    public Integer artist;
    public static  Finder<Integer, Song> find = new Finder<>(Song.class);

   /* public Song(Integer id,String title, String url, Integer artist) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.artist = artist;
    } */
}
