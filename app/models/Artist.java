package models;
import io.ebean.*;
import org.springframework.context.annotation.Primary;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import javax.validation.Constraint;
import java.util.HashSet;
import java.util.*;
/**
 * User entity managed by Ebean.
 * @author David Gutierrez
 */
@Entity
public class Artist extends Model{
    @Id
    public Integer id;
    //@Constraints.Required
    public String email;
    //@Constraints.Required
    public String username;
    //@Constraints.Required
    public String first_name;
    //@Constraints.Required
    public String last_name;
    //@Constraints.Required
    public String password;
    public Integer age;
    public String music;
    public static  Finder<Integer, Artist> find = new Finder<>(Artist.class);

 /*   public Artist(Integer id,String email,String username,String first_name,String last_name,String password, Integer age, String music) {
        this.id = id;
        this.email = email;
        this.username = username;
        this.first_name = first_name;
        this.last_name = last_name;
        this.password = password;
        this.age = age;
        this.music = music;
    }*/
}