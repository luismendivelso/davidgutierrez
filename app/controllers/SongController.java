package controllers;
import models.Artist;
import models.Song;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import  views.html.songs.*;
import  views.html.*;
import views.html.songs.index;
import views.html.songs.show;

import javax.inject.Inject;
/**
 * This controller contains the actios methods of Song model
 * @author David Gutirrez
 */
public class SongController extends Controller {
    @Inject
    FormFactory formFactory;
//import com.typesafe.sbt.packager.MappingsHelper._
// mappings in Universal ++= directory(baseDirectory.value / "public")
    /**
     * for search all songs.
     * @author David Gutierrez
     */
    public Result index() {
        List<Song> songs = Song.find.all();
        List<Artist> artists = Artist.find.all();
        return ok(index.render(songs, artists));
    }
    /**
     * this method create a song.
     * @author David Gutierrez
     */
    public Result create() {
        Form<Song> songForm = formFactory.form(Song.class);
        List<Artist> artists = Artist.find.all();
        return ok(views.html.songs.create.render(songForm,artists));
    }
    /**
     * this method uploads the song file.
     * @author David Gutierrez
     */
    /*
    public Result uploadd() {
        File file = request().body().asRaw().asFile();
        File newFile = new File("./public/songs" , ".mp3");
        file.renameTo(newFile);
        return ok("File uploaded");
    }
    */
    /**
     * this method saves the song file.
     * @author David Gutierrez
     */
    public Result save() {
        Form<Song> SongForm = formFactory.form(Song.class).bindFromRequest();
        Song song = SongForm.get();
        //final Http.MultipartFormData<File> formData = request().body().asMultipartFormData();
        //final Http.MultipartFormData.FilePart<File> filePart = formData.getFile("file");
        //final File file = filePart.getFile();
        //File newFile = new File ("./public/songs", song.title +".mp3" );
        //file.renameTo(newFile);
        //String songName= song.title+".mp3";
        //song.url= songName ;
        song.save();
        return redirect(routes.SongController.index());
    }

    /**
     * this method elminates the song selected by id.
     * @author David Gutierrez
     */
    public Result delete(Integer id) {
        Song song= Song.find.byId(id);
        if (song == null) {
            return notFound(views.html.errors._404.render());
        }
        song.delete();
        return redirect(routes.SongController.index());
    }
    /**
     * this method shows the song information.
     * @author David Gutierrez
     */
    public Result show(Integer id) {
        Song song= Song.find.byId(id);
        List<Artist> artists = Artist.find.all();
        if (song == null) {
            return notFound(views.html.errors._404.render());
        }
        return ok(show.render(song, artists));
    }
/*
    public Result upload() {
        Http.MultipartFormData<File> body = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart<File> song = body.getFile("song");
        if (song != null) {
            String fileName = song.getFilename();
            String contentType = song.getContentType();
            File file = song.getFile();
            return ok("File uploaded");
        } else {
            flash("error", "Missing file");
            return badRequest();
        }
    }*/

/*
    //to edit a single artist
    public Result edit(Integer id) {
        Song song = Song.find.byId(id);
        if (song == null) {
            return notFound("The Artist is not found");
        }


        Form<Song> songForm= formFactory.form(Song.class).fill(song);
        return ok(edit.render(songForm));
    }
*/
  /*  //to update the single artist edited
    public Result update() {
        Form<Song> songForm= formFactory.form(Song.class).bindFromRequest();


        if (songForm.hasErrors()){
            flash("danger","Please Correct The Form Below");
            return badRequest(edit.render(songForm));
        }

        Song song = songForm.get();
        Song oldSong= song.find.byId(song.id);
        if (oldSong == null) {
            return notFound("Artist not found");
        }

        oldSong.title = song.title;
        oldSong.url= song.url;
        oldSong.artist= song.artist;
        oldSong.update();
        flash("success","Artist Updated Successfuly");
        return redirect(routes.SongController.index());
    }
*/

}

