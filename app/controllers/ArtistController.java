package controllers;
import models.Artist;
import models.Song;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.artists.index;
import java.util.List;
import java.util.Set;
import  views.html.artists.*;
import  views.html.*;
import  views.html.errors.*;

import javax.inject.Inject;

/**
 * This controller contains the CRUD actios of artist model
 * @author David Gutirrez
 */
public class ArtistController extends Controller{

    @Inject
    FormFactory formFactory;

    /**
     * this method searches for all artists.
     * @author David Gutierrez
     */
    public Result index() {
        List<Artist> artists = Artist.find.all();
        return ok(index.render(artists));
    }

    /**
     * this method creates an artist.
     * @author David Gutierrez
     */
    public Result create() {
        Form<Artist> artistForm = formFactory.form(Artist.class);
        return ok(create.render(artistForm));
    }

    /**
     * this method saves an artist.
     * @author David Gutierrez
     */
    public Result save() {
        Form<Artist> artistForm = formFactory.form(Artist.class).bindFromRequest();
        if (artistForm.hasErrors()){
            flash("danger", "Please Correct The Form Below");
            return badRequest(create.render(artistForm));
        }
        Artist artist = artistForm.get();
        artist.save();
        flash("success", "Artist Save Successfuly");
        return redirect(routes.ArtistController.index());
    }

    /**
     * this method edits an artist.
     * @param id
     * @author David Gutierrez
     */
    public Result edit(Integer id) {
        Artist artist = Artist.find.byId(id);
        if (artist == null) {
            return notFound(views.html.errors._404.render());
        }


        Form<Artist> artistForm = formFactory.form(Artist.class).fill(artist);
        return ok(edit.render(artistForm));
    }
    /**
     * this method updates a single edited artist.
     * @author David Gutierrez
     */
    public Result update() {
        Form<Artist> artistForm = formFactory.form(Artist.class).bindFromRequest();


        if (artistForm.hasErrors()){
            flash("danger","Please Correct The Form Below");
            return badRequest(edit.render(artistForm));
        }

        Artist artist = artistForm.get();
        Artist oldArtist = artist.find.byId(artist.id);
        if (oldArtist == null) {
            return notFound(views.html.errors._404.render());
        }

        oldArtist.email = artist.email;
        oldArtist.username = artist.username;
        oldArtist.first_name = artist.first_name;
        oldArtist.last_name = artist.last_name;
        oldArtist.password = artist.password;
        oldArtist.age = artist.age;
        oldArtist.music = artist.music;
        oldArtist.update();
        flash("success","Artist Updated Successfuly");
        return redirect(routes.ArtistController.index());
    }

    /**
     * this method eliminates a single artist by id.
     * @param id
     * @author David Gutierrez
     */
    public Result delete(Integer id) {
        Artist artist = Artist.find.byId(id);
        if (artist == null) {
            return notFound(views.html.errors._404.render());
        }
        artist.delete();
        return redirect(routes.ArtistController.index());
    }
    /**
     * this method shows the details of a selected artist.
     * @param id
     * @author David Gutierrez
     */
    public Result show(Integer id) {
        Artist artist = Artist.find.byId(id);
        List<Song> songs = Song.find.all();
        if (artist == null) {
            return notFound(views.html.errors._404.render());
        }
        return ok(views.html.artists.show.render(artist,songs));
    }
}
