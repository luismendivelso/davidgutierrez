# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table artist (
  id                            integer auto_increment not null,
  email                         varchar(255),
  username                      varchar(255),
  first_name                    varchar(255),
  last_name                     varchar(255),
  password                      varchar(255),
  age                           integer,
  music                         varchar(255),
  constraint pk_artist primary key (id)
);

create table song (
  id                            integer auto_increment not null,
  title                         varchar(255),
  url                           varchar(255),
  artist                        integer,
  constraint pk_song primary key (id)
);


# --- !Downs

drop table if exists artist;

drop table if exists song;

