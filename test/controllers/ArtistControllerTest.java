package controllers;
import models.Artist;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;
import org.junit.Test;
import play.mvc.Result;
import play.twirl.api.Content;
import static org.junit.Assert.*;

public class ArtistControllerTest {

    @Test
    public void index() {
        Result result = new ArtistController().index();
        assertEquals(OK, result.status());
        assertEquals("text/html", result.contentType().get());
        assertEquals("utf-8", result.charset().get());
        assertTrue(contentAsString(result).contains("Welcome"));
    }

}