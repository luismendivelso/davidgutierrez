package models;
import com.sun.xml.internal.bind.v2.TODO;
import javafx.beans.binding.ObjectExpression;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.assertThat;
import play.data.validation.Constraints;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static play.test.Helpers.*;

public class ArtistTest {
    @Test
    public void testArtistModel(){
        running(fakeApplication(inMemoryDatabase()), new Runnable(){
            @Override
            public void run(){
                Integer id =4;
                String username="dada";
                String email="asa@gmai.com";
                String first_name= "asas";
                String last_name="asas";
                String password = "ole";
                Integer age=21;
                String music="rock";
                Artist artist = new Artist(id,username,email,first_name,last_name,password, age, music );
                artist.save();

                Artist saveArtist = Artist.find.byId(artist.id);
                assertThat(saveArtist, allOf(equalTo(null)));
                assertThat(saveArtist.username, equalTo(username));
                assertThat(saveArtist.email, equalTo(email));
                assertThat(saveArtist.first_name, equalTo(first_name));
                assertThat(saveArtist.last_name, equalTo(last_name));
                assertThat(saveArtist.password, equalTo(password));
                assertThat(saveArtist.age, equalTo(age));
                assertThat(saveArtist.music, equalTo(music));
                //assertFalse(saveArtist.isEmpty());
                /*assertEquals(id, saveArtist.id);
                assertEquals(username, saveArtist.username); //saveArtist.username == username;
                assertEquals(email, saveArtist.email);
                assertEquals(first_name, saveArtist.first_name);
                assertEquals(last_name, saveArtist.last_name);
                assertEquals(password, saveArtist.password);
                assertEquals(age, saveArtist.age);
                assertEquals(music, saveArtist.music);
*/

            }
        });
    }
}